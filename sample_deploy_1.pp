# puppet apply sample_deplo1.pp
# sudo /opt/puppetlabs/puppet/bin/puppet apply sample_deploy_1.pp

# https://forge.puppet.com/puppetlabs/tomcat
# puppet module upgrade puppetlabs-stdlib
#instalando modulo tomcat
# sudo /opt/puppetlabs/puppet/bin/puppet module install puppetlabs/tomcat

java::adopt { 
  'jdk14_unistall' :
  ensure  => 'absent',
  version => '8',
  java => 'jdk',
}

java::adopt { 
  'jdk14_install' :
  ensure  => 'present',
  version => '8',
  java => 'jdk',
}

# instalar nginx
tomcat::install { 
  '/opt/tomcat':
  source_url => 'http://mirror.nbtelecom.com.br/apache/tomcat/tomcat-9/v9.0.35/bin/apache-tomcat-9.0.35.tar.gz',
}

tomcat::instance { 
  'default':
  catalina_home => '/opt/tomcat',
}

# garantir que o App Api-Investimentos esta no diretorio correto
file {
  'api_investimento_app_present':
  path => "/home/ubuntu/webapps/Api-Investimentos-0.0.1-SNAPSHOT.jar",
  ensure => "present"
}

# servico para iniciar a aplicacao java
service { 
  "apiinvestimentos":
  ensure     => running,
  enable     => true,
}


